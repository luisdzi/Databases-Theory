There are made two algorithms for database normalization in his project.
One is use to obtain the closure of a set of functional dependencies
 determined by a subset of attributes. The other one is to determine a set of candidate keys given a relationship and a set of functional dependencies. Both algorithms are implemented in Haskell.